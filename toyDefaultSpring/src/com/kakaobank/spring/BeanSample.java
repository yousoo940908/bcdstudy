package com.kakaobank.spring;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.kakaobank.spring.bean.annotation.EmpService2;
import com.kakaobank.spring.bean.annotation.config.SpringConfig;
import com.kakaobank.spring.bean.xml.EmpService;

public class BeanSample {
	public static void main(String[] args) {
//		1. XML 설정 방식
		ApplicationContext ctx = new ClassPathXmlApplicationContext("application.xml");
		EmpService empService = (EmpService) ctx.getBean("empService");
		empService.print();
		
//		2. annotation 설정 방식
//		ApplicationContext ctx = new AnnotationConfigApplicationContext(SpringConfig.class);
//		EmpService2 empService = (EmpService2) ctx.getBean("empService");
//		empService.print();
	}
}
