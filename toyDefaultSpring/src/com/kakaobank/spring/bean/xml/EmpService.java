package com.kakaobank.spring.bean.xml;

public class EmpService {
	private TestMapper mapper;
	private String name ;
	private int age ;
	
	public void print() {
		mapper.print();
		System.out.println(this.name);
		System.out.println(this.age);
	}
	
	
//	1. 생성자 주입
	public EmpService(String name, int age) {
		this.name = name;
		this.age = age;
	}


	public TestMapper getMapper() {
		return mapper;
	}


	public void setMapper(TestMapper mapper) {
		this.mapper = mapper;
	}
	
//	2. setter 주입
//	public String getName() {
//		return name;
//	}
//	public void setName(String name) {
//		this.name = name;
//	}
//	public int getAge() {
//		return age;
//	}
//	public void setAge(int age) {
//		this.age = age;
//	}
	
	
	
	
	
	
	
}
