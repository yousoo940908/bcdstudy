package com.kakaobank.spring.bean.annotation.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.kakaobank.spring.bean.annotation")
public class SpringConfig {

}
