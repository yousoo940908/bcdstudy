package com.kakaobank.spring.bean.annotation;

import org.springframework.stereotype.Repository;

@Repository
public class TestMapper2 {
	private String mapperName = "원동혁바보";

	
	public void print() {
		System.out.println("매퍼명 :: "+mapperName);
	}
	
	public String getMapperName() {
		return mapperName;
	}

	public void setMapperName(String mapperName) {
		this.mapperName = mapperName;
	}
	
	

}
