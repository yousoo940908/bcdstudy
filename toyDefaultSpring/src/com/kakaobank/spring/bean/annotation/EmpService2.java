package com.kakaobank.spring.bean.annotation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service("empService")
public class EmpService2 {
	private TestMapper2 mapper2;
	
	@Autowired
	public EmpService2(TestMapper2 mapper2) {
		this.mapper2 = mapper2;
	}
	
	
	public void print() {
		mapper2.print();
	}

}
