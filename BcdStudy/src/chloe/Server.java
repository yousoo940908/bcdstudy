package chloe;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FilenameFilter;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URLDecoder;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List ;

public class Server {
	public static void main(String[] args) {
		
		ServerSocket listener = null;
		
        try{
            //특정 port에서 기다리는 ServerSocket을 생성한다.
            listener = new ServerSocket(8080);
            System.out.println("client를 기다립니다.\n");
            

            //소켓서버 종료될때까지 무한루프
            while(true){

                //클라이언트와 통신하기 위한 객체
                Socket client = listener.accept(); //클라이언트가 접속할때까지 기다린다.
                System.out.println("client 접속 : " + client);
                
                OutputStream out = client.getOutputStream();  //클라이언트에게 보낼 통로를 만들어줌
                InputStream in = client.getInputStream();
                System.out.println("OutputStream (보내는)정보 : " + out + "\n");
                
                byte[] bytebuffer = new byte[5*1024]; // a read buffer of 5KiB
                client.getInputStream().read(bytebuffer);
                
                String stringData = new String(bytebuffer,"UTF-8");
                               
                
                String responseData = "HTTP/1.1 200 OK\r\n" +
                        "Server: \r\n" +
                        "Content-type: text/html\r\n\r\n" +
                        "<HTML>" +
                        "<HEAD> <meta charset=\"utf-8\"> <TITLE>200</TITLE> </HEAD>" // <HEAD> 태그안에 <meta charset~>을 삽입해 한글깨짐 해결
                        ;
                
                System.out.println("===========로그시작===========");
                System.out.println(stringData);
                
                
                if(stringData.contains("Sec-Fetch-Dest: image")) {
                	
                	System.out.println(">>> 이미지 요청입니다.");
                	image(stringData, out);
                	
                    
                } else if(stringData.contains("message=")) {

                	System.out.println(">>> form 요청입니다.");
                	message(stringData, responseData, out);
                    
                } else if(stringData.contains("GET /ABC HTTP/1.1")) {
                	
                	System.out.println(">>> 에러페이지");
                	error(responseData, out);
                    
                    
                } else if(stringData.contains("GET /fileForm")) {  //파일 검색 요청
                	//GET /fileForm?file=heart HTTP/1.1
                	
                	int index = stringData.indexOf("file=");
                	int index2 = stringData.indexOf("HTTP/1.1");
                	String fileName = stringData.substring(index+5, index2-1).trim();
                	System.out.println(">>> file 검색 요청입니다. >> " + fileName);
                	
                	file(responseData, fileName, out);
                	
                } else if(stringData.contains("GET / HTTP/1.1")){  //text
                	
                	System.out.println(">>> text 요청입니다.");
                	text(responseData, out);
                }
                System.out.println("=========== 로그끝 ===========\n");
                
                
                // 반드시, Response Header를 보낸 후에 닫아야함
                in.close();
                out.close();  //호스를 닫음
                client.close();  //소켓 문을 닫음
            }
        }catch(Exception e){
            e.printStackTrace();
        }finally { //finally부분에서 서버소켓을 close한다.
            try {
                listener.close();  //방 조차도 닫아서 종료 (서버 종료)
            }catch(Exception e) {}
        }
		
	}
	
	
	
	public static void text(String responseData, OutputStream out) throws Exception {
		responseData += "<BODY>바보!<br>Hello World!" +
                "<br><br><< 이미지 >><br><img src=\"/Users/kakaobank1/Downloads/heart.jpg\"/><br><br>" +
                "<form action = 'textForm' accept-charset='utf-8' method = 'post'>" +
                	"<input type = 'text' name = 'message'> <br>" +
                	"<input type = 'submit' value='제출'>" +
                "</form>" + 
                "<form action = 'fileForm' accept-charset='utf-8' method = 'get'>" +
                	"<input type = 'text' name = 'file'> <br>" +
                	"<input type = 'submit' value='파일검색'>" +
                "</form><br>" + 
                "</BODY>" +
                "</HTML>";
		out.write(responseData.getBytes());
	}
	
	//이미지만 뿌려주는 애
	public static void image(String stringData, OutputStream out) throws Exception {
		
		int index1 = stringData.indexOf("GET");
    	int index2 = stringData.indexOf("Host");
    	System.out.println("index1 : "+index1+", index2 : "+index2);
    	String filePath = stringData.substring(index1+4, index2-11);
    	System.out.println("filePath : " + filePath);
    	
    	//파일
    	File file = new File(filePath);
    	FileInputStream fis = new FileInputStream(file);
    	
        byte[] bytefile = new byte[(int) file.length()];
        bytefile = Files.readAllBytes(file.toPath());  //파일을 바이트 배열로 변환
        
        fis.read(bytefile);
        fis.close();
        
        DataOutputStream binaryOut = new DataOutputStream(out);
        binaryOut.writeBytes("HTTP/1.0 200 OK\r\nServer: \r\n");
        binaryOut.writeBytes("Content-Type: image/png\r\n");
        binaryOut.writeBytes("Content-Length: " + bytefile.length);
        binaryOut.writeBytes("\r\n\r\n");
    	
        binaryOut.write(bytefile);
        binaryOut.close();
        
	}
	
	public static void message(String stringData, String responseData, OutputStream out) throws Exception {
    	
		int index = stringData.indexOf("message=");
    	String formData = stringData.substring(index+8);
    	
    	responseData += "<BODY>바보!<br>Hello World!" +
                "<br><br><< 이미지 >><br><img src=\"/Users/kakaobank1/Downloads/heart.jpg\"/><br><br>" +
                "<form action = 'textForm' accept-charset='utf-8' method = 'post' >" +
                	"<input type = 'text' name = 'message'> <br>" +
                	"<input type = 'submit' value='제출'><br>" +
                "</form>" + 
                formData + 
                "<br><br><form action = 'fileForm' accept-charset='utf-8' method = 'get'>" +
                	"<input type = 'text' name = 'file'> <br>" +
                	"<input type = 'submit' value='파일검색'>" +
                "</form><br>" + 
                "</BODY>" +
                "</HTML>";
    	URLDecoder.decode(responseData, "UTF-8");
        out.write(responseData.getBytes());
	}
	
	public static void error(String responseData, OutputStream out) throws Exception {
		
		responseData += "<BODY> <h1>404 Error!!!</h1><br>" +
                "<a href='http://localhost:8080'>Go Home</a>" +
                "</form>" + 
                "</BODY>" +
                "</HTML>";
        out.write(responseData.getBytes());
		
	}
	
	public static void file(String responseData, String fileRequest, OutputStream out) throws Exception {

    	// (File file, String name)
    	//FileFilter filter = f -> f.getName().endsWith(".png");
		
		//해당 경로에 있는 파일들을 담음
    	File directory = new File("/Users/kakaobank1/Downloads");
    	
    	FilenameFilter filenameFilter = new FilenameFilter() {
    		public boolean accept(File f, String name) {
    			return name.endsWith(".jpg");
    		}
    	};
    	
    	//directory 내 파일목록을 String 배열에 담아줌 (확장자 필터링해서)
    	//String[] fileArray = directory.list((f,name)->name.endsWith(".jpg"));
    	String[] fileArray = directory.list(filenameFilter);
    	
    	//파일목록 String 배열 -> List로 변환
    	List<String> fileList = new ArrayList<>(Arrays.asList(fileArray));
    	for(String str : fileList) {
    		System.out.println("디렉토리 리스트 출력 : " + str);
    		
    	}
    	
    	//formData로 요청받은 file 네임
    	fileRequest = fileRequest.concat(".jpg");
    	
    	responseData += "<BODY>바보!<br>Hello World!" +
                "<br><br><< 이미지 >><br><img src=\"/Users/kakaobank1/Downloads/heart.jpg\"/><br><br>" +
                "<form action = 'textForm' accept-charset='utf-8' method = 'post' >" +
                	"<input type = 'text' name = 'message'> <br>" +
                	"<input type = 'submit' value='제출'><br>" +
                "</form>" + 
                "<br><form action = 'fileForm' accept-charset='utf-8' method = 'get'>" +
                	"<input type = 'text' name = 'file'> <br>" +
                	"<input type = 'submit' value='파일검색'>" +
                "</form><br>";
    	
    	if(fileList.contains(fileRequest)) {
    		System.out.println("일치하는 파일이 존재합니다.");
    		
    		String filePath = "/Users/kakaobank1/Downloads/"+fileRequest;
            responseData += "파일을 찾았습니다.<br>" +
                    "<img src=" + filePath + "/><br>" +
                    "파일경로 : " + filePath +
                    "</BODY>" +
                    "</HTML>";
    	}else {
    		System.out.println("일치하는 파일이 존재하지 않습니다.");
    		
    		responseData += "파일이 존재하지 않습니다." + 
                    "</BODY>" +
                    "</HTML>";
    	}
    	out.write(responseData.getBytes());
		
	}
	
}

