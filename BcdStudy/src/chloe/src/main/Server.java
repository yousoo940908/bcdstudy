package chloe.src.main;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;

import chloe.src.util.RequestHandler;
import chloe.src.util.ResponseHandler;

public class Server {
	public static void main(String[] args) {

		ServerSocket serverSocket = null;

		try {
			// 특정 port에서 기다리는 ServerSocket을 생성한다.
			serverSocket = new ServerSocket(1111);
			System.out.println("client를 기다립니다.");
			
			while (true) {
				// 클라이언트와 통신하기 위한 객체
				Socket client = serverSocket.accept(); // 클라이언트가 접속할때까지 기다린다.
				System.out.println("\nclient 접속 : " + client);

				OutputStream out = client.getOutputStream(); // 클라이언트에게 보낼 통로를 만들어줌
				InputStream in = client.getInputStream();
				
				// HttpRequest 파싱, 분기
				RequestHandler requestHandler = new RequestHandler();
				HashMap<String, String> httpRequest = requestHandler.requestParser(in);
				HashMap<String, String> response = requestHandler.requestHandler(httpRequest);

				// HttpResponse 조립
				ResponseHandler responseHandler = new ResponseHandler();
				responseHandler.responseHandler(response, out);

				
				// HashMap<String, String> request = requestHandler.httpRequest(in);
				// String response = requestHandler.requestParser(request);

				// out.write(response.getBytes());

				in.close();
				out.close();
				client.close();
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				serverSocket.close(); // 방 조차도 닫아서 종료 (서버 종료)
			} catch (Exception e) {
			}
		}

	}
}
