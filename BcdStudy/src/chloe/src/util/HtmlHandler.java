package chloe.src.util;

public class HtmlHandler {

	String html;
	
	String header = 
			"HTTP/1.1 200 OK\r\n" + 
			"Server: \r\n";

	String homeHtml = 
			"<HTML>" + 
			"<HEAD> <meta charset=\"utf-8\"> <TITLE>200</TITLE> </HEAD>" +
			"<BODY>" + 
				"Hello World!<br><br>" + 
				"<img src='heart.jpg'/><br><br>" +

				"<form action = 'textForm' accept-charset='utf-8' method = 'post'>" + 
					"<input type = 'text' name = 'message'> <br>" + 
					"<input type = 'submit' value='제출'>" + 
				"</form>" +

				"<br><form action = 'fileForm' accept-charset='utf-8' method = 'get'>" + 
					"<input type = 'text' name = 'file'> <br>" + 
					"<input type = 'submit' value='파일검색'>" + 
				"</form><br>" + 
			"</BODY>" + 
			"</HTML>";

	String errorHtml = 
			"<HTML>" + 
			"<BODY>" + 
				"<h1>404 Error!!!</h1><br>" + 
				"<a href='http://localhost:1111'>Go Home</a>" + 
			"</BODY>" + 
			"</HTML>";

	
	// httpRequest의 fetchDest가 document인 애들 헤더 처리 (home, url, getForm, postForm)
	public String documentHeader() {
		header += "Content-type: text/html\r\n\r\n";
		return header;
	}
	// httpRequest의 fetchDest가 image인 애들 헤더 처리
	public String imageHeader(String contentLength) {
		header += "Content-type: image/png\r\n";
		header += "Content-Length: " + contentLength + "\r\n";
		return header;
	}
	
	public String homeHtml() {
		html = documentHeader() + homeHtml;
		
		return html;
	}
	
	public String errorHtml() {
		html = documentHeader() + errorHtml;
		return html;
	}
	
	public String formHtml(String action, String name, String data) {
		html = formInsert(action, name, data);
		return html;
	}
	
	public String fileFormHtml(String action, String name, String data) {
		String imageTag = "<img src='" + data + ".jpg'/><br>";
		
		html = formInsert(action, name, "요청한 파일 : " + data + "<br>파일을 찾았습니다.<br>" + imageTag);
		return html;
	}
	
	public String fileFailFormHtml(String action, String name, String data) {
		html = formInsert(action, name, "요청한 파일 : " + data + "<br>파일이 존재하지 않습니다.");
		return html;
	}
	
	public String formInsert(String action, String name, String data) {
		html = documentHeader();
		// action?name=data
		int index1 = homeHtml.indexOf("<form action = '" + action + "'");
		int index2 = homeHtml.indexOf("name = '" + name + "'", index1);
		int index3 = homeHtml.indexOf("</form>", index2) + 7;

		String str1 = homeHtml.substring(0, index3) + data;
		String str2 = homeHtml.substring(index3);
		
		html = html + str1.trim() + str2.trim();
		
		return html;
	}
	
}
