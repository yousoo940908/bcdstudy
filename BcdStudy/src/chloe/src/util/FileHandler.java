package chloe.src.util;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FilenameFilter;
import java.io.OutputStream;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FileHandler {

	public void image(String filePath, OutputStream out) throws Exception {
		
		File file = new File(filePath);
		FileInputStream fis = new FileInputStream(file);

		byte[] bytefile = new byte[(int) file.length()];
		bytefile = Files.readAllBytes(file.toPath()); // 파일을 바이트 배열로 변환

		fis.read(bytefile);
		fis.close();

		DataOutputStream binaryOut = new DataOutputStream(out);
		binaryOut.writeBytes("HTTP/1.0 200 OK\r\nServer: \r\n");
		binaryOut.writeBytes("Content-Type: image/png\r\n");
		binaryOut.writeBytes("Content-Length: " + bytefile.length);
		binaryOut.writeBytes("\r\n\r\n");

		binaryOut.write(bytefile);
		binaryOut.close();
	}
	
	public boolean fileSearch(String fileName) throws Exception{
		
		// (File file, String name)
    	//FileFilter filter = f -> f.getName().endsWith(".png");
		
		//해당 경로에 있는 파일들을 담음
    	File directory = new File("/");
    	
    	FilenameFilter filenameFilter = new FilenameFilter() {
    		public boolean accept(File f, String name) {
    			return name.endsWith(".jpg");
    		}
    	};
    	
    	//directory 내 파일목록을 String 배열에 담아줌 (확장자 필터링해서)
    	//String[] fileArray = directory.list((f,name)->name.endsWith(".jpg"));
    	String[] fileArray = directory.list(filenameFilter);
    	
    	
    	//파일목록 String 배열 -> List로 변환
    	List<String> fileList = new ArrayList<>(Arrays.asList(fileArray));
    	for(String str : fileList) {
    		System.out.println("디렉토리 리스트 출력 : " + str);
    		
    	}
    	
    	//formData로 요청받은 file 네임
    	String file = fileName.concat(".jpg");
    	boolean fileExist = false;
    	
    	if(fileList.contains(file)) {
    		System.out.println("파일이 존재합니다.");
    		fileExist = true;
    	}else {
    		System.out.println("파일이 존재하지 않습니다.");
    		fileExist = false;
    	}
    	
		return fileExist;
	}

}
