package chloe.src.util;

import java.io.OutputStream;
import java.util.HashMap;

public class ResponseHandler {
	
	String type;
	String request;
	String action;
	String name;
	String data;
	String filePath;
	String contentLength;
	String url;
	
	public void responseHandler(HashMap<String, String> response, OutputStream out) throws Exception {
		
		// httpRequest : method, request, fetchDest, contentLength
		// response : type, action, data, request
		
		// request 내용 종류 : 빈칸, aa?bb=cc, /url, file경로, /url
		// type = home, getForm, url, image, postForm
		
		type = response.get("type");
		request = response.get("request");
		action = response.get("action");
		name = response.get("name");
		data = response.get("data");
		filePath = response.get("filePath");
		contentLength = response.get("contentLength");
		url = response.get("url");

		HtmlHandler htmlHandler = new HtmlHandler();
		String httpResponse = "";
		
		//type = home, getForm, url, image, postForm
		
		if (type.equals("home")) {
			httpResponse = htmlHandler.homeHtml();  // home.html 로 보내버림 (해당 html 그대로 사용)
			out.write(httpResponse.getBytes());  //전송
			
		} else if (type.equals("url")) {  //지금은 error 페이지 html
			if(url.equals("ABC")) {
				httpResponse = htmlHandler.errorHtml();
			}else {  //ABC 이외 url도 일단 다 에러페이지로 가게 만들었음
				httpResponse = htmlHandler.errorHtml();
			}
			out.write(httpResponse.getBytes());  //전송

		} else if (type.equals("getForm")) {
			httpResponse = htmlHandler.formHtml(action, name, data);
			out.write(httpResponse.getBytes());  //전송

		} else if (type.equals("image")) {
			//httpResponse = htmlHandler.imageHeader(contentLength);
			
			if(!request.equals("/favicon.ico")) {
				FileHandler fileHandler = new FileHandler();
				fileHandler.image(filePath, out);  //이미지 클라이언트에게 전송해줌
			}

		} else if (type.equals("fileSearch")) {
			FileHandler fileHandler = new FileHandler();
			boolean fileExist = fileHandler.fileSearch(data);
			
			if(fileExist) {
				httpResponse = htmlHandler.fileFormHtml(action, name, data);  //파일 경로를 가진 이미지태그를 html에 넣어서 보내줌
			}else {
				httpResponse = htmlHandler.fileFailFormHtml(action, name, data);
			}
			out.write(httpResponse.getBytes());  //전송

		} else if (type.equals("postForm")) {
			httpResponse = htmlHandler.formHtml(action, name, data);
			
			out.write(httpResponse.getBytes());  //전송
			
		}
		

	}

}
