package chloe.src.util;

import java.io.InputStream;
import java.util.HashMap;

public class RequestHandler {

	String method;
	String request;
	String fetchDest;
	String contentLength;
	
	String type;
	String action;  //url
	String name;
	String data;
	String filePath;
	String url;
	
	
	public HashMap<String, String> requestHandler(InputStream in) throws Exception {
		HashMap<String, String> httpRequest = requestParser(in);
		return requestHandler(httpRequest);
	}
	
	// http request 파싱
	public HashMap<String, String> requestParser(InputStream in) throws Exception {
		
		byte[] bytebuffer = new byte[5 * 1024]; // a read buffer of 5KiB
		in.read(bytebuffer);

		// request 출력
		String stringData = new String(bytebuffer, "UTF-8");
		System.out.println(stringData);

		method = stringData.substring(0, stringData.indexOf(" "));
		request = stringData.substring(stringData.indexOf(" ")+1, stringData.indexOf(" HTTP/1.1"));  //home일 경우 /만 들어있음
		
		int index = stringData.indexOf("Sec-Fetch-Dest:") + 16;
		fetchDest = stringData.substring(index, stringData.indexOf("\n", index)-1);
		
		index = stringData.indexOf("Content-Length:");  //해당 문자열이 없으면 -1 반환
		if(index != -1) {
			contentLength = stringData.substring(index+16, stringData.indexOf("\n", index)-1);
		}
		//POST로 formData 요청 왔을때 -> request 하단에 요청 데이터 나옴 (message=aaa)
		int index1 = stringData.indexOf("Accept-Language: ko-KR,ko;q=0.9")+35;  //message 시작부분
		int index2 = stringData.indexOf("=", index1);  //= 시작부분
		if(index2 != -1) {
			name = stringData.substring(index1, index2);
			data = stringData.substring(index2 + 1);
		}
		
		System.out.println("method : " + method + "\nrequest : " + request + "\nfetchDest : " + fetchDest);
		System.out.println("contentLength : " + contentLength + "\nname : " + name + "\ndata : " + data);
		
		HashMap<String, String> map = new HashMap<>();
		map.put("method", method);
		map.put("request", request);
		map.put("fetchDest", fetchDest);
		map.put("contentLength", contentLength);
		map.put("name", name);
		map.put("data", data);
		
		return map;
	}

	// 분기에 따라 요청 처리
	public HashMap<String, String> requestHandler(HashMap<String, String> httpRequest) throws Exception {

		method = httpRequest.get("method");
		request = httpRequest.get("request");
		fetchDest = httpRequest.get("fetchDest"); // document, image
		contentLength = httpRequest.get("contentLength");
		name = httpRequest.get("name");
		data = httpRequest.get("data");

		HashMap<String, String> response = new HashMap<>();
		

		// header 정보에 따라 분기
		if(method.equals("GET")) { // GET : 기본 home, 이미지 처리 요청, 에러페이지
			
			if (fetchDest.equals("document")) {  //기본 home, get <form> 조회
				
				if (request.equals("/")) {
					type = "home"; // html 파일만 내려줌
				}else if(request.contains("?")) {  // get 메서드로 form 조회
					type = "getForm";
					
					int index1 = request.indexOf("?");
					action = request.substring(1, index1);  //어느 url로 폼데이터를 보낼건지 (도착하는 url)
					
					int index2 = request.indexOf("=");
					name = request.substring(index1 +1, index2);
					data = request.substring(index2 + 1);
					if(name.equals("file")) {  //fileSearch 요청
						type = "fileSearch";
					}
				}else {
					type = "url";
					url = request.substring(1);  //ABC
					//해당 url에 대한 처리
				}

			}else if (fetchDest.equals("image")) {  //이미지처리
				type = "image";
				filePath = request;
			}
			
		}else if (method.equals("POST")) {  // POST 메서드로 form 조회 >> HTTP Request의 헤더부분에 request내용 정보 없고 맨 하단에 존재
			type = "postForm";
			
			action = request.substring(1);  //url
		}
		
		response.put("request", request);
		response.put("type", type);

		response.put("action", action);
		response.put("name", name);
		response.put("data", data);
		response.put("filePath", filePath);
		response.put("contentLength", contentLength);
		response.put("url", url);
		
		System.out.println(">>> " + type + " 요청입니다.");

		return response;
	}

}
