package soo.http;

import java.io.IOException;
import java.io.InputStream;

import soo.util.HeaderParser;


public class HttpRequest {
	private String method = "";
	private String url = "";
	private String postUrl = "SOO";
	private String postResult = "";
	private String getParam = "";
	private String getValue = "";
	
	public void receive(InputStream in) throws IOException {
		HeaderParser hp = new HeaderParser();
		hp.parsing(in, this);
		
		System.out.println("---- method : " + this.getMethod() + ", url : " + this.getUrl());
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getPostUrl() {
		return postUrl;
	}

	public void setPostUrl(String postUrl) {
		this.postUrl = postUrl;
	}

	public String getPostResult() {
		return postResult;
	}

	public void setPostResult(String postResult) {
		this.postResult = postResult;
	}

	public String getGetParam() {
		return getParam;
	}

	public void setGetParam(String getParam) {
		this.getParam = getParam;
	}

	public String getGetValue() {
		return getValue;
	}

	public void setGetValue(String getValue) {
		this.getValue = getValue;
	}

	
}

