package soo.http;

import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;

import soo.util.FileLoad;
import soo.util.XssFilter;

public class HttpResponse {
	String fileRegExp = "^([\\S]+(\\.(?i)(jpg|png|jpeg))$)";
	HttpRequest hr;
	String msg = "";
	String fileSearchResult = "";
	
	public HttpResponse(HttpRequest hr) {
		this.hr = hr;
	}

	public void send(Socket socket, OutputStream out) throws IOException {
		XssFilter xssFilter = new XssFilter();
		

		out = socket.getOutputStream();
		out.write(new String ("HTTP/1.1 200 OK\r\n").getBytes()); 

		//GET REQUEST
		if("GET".equals(hr.getMethod())) {
			if(hr.getUrl().matches(fileRegExp)) {
				FileLoad.fileOutputStreamSetting(socket, out, hr.getUrl());
			}
			if(hr.getUrl().equals(hr.getPostUrl())) {
				errorBodySetting();
			} 
			if(hr.getGetValue() != "") {
				fileSearchResult = FileLoad.findFileList(hr.getGetValue());
			}
		}
		//POST REQUEST
		if("POST".equals(hr.getMethod())) {
		}

		httpBodySetting();

		msg = xssFilter.cleanXSS(msg);

		out.write(new String ("Content-Length:"+msg.getBytes("utf-8").length+"\r\n").getBytes());
		out.write(new String ("Content-Type: text/html;charset=UTF-8\r\n\r\n").getBytes());
		out.write(msg.getBytes("utf-8"));

		out.flush();
		out.close();
	}

	public void httpBodySetting() {
		msg = "";

		msg += "Hello Soo~ #HW_4<br>";

		msg += "<img src='bread.png'><br>";

		msg += "<form action='"+ hr.getPostUrl() +"' method='POST'><br>";
		msg += "** POST URL TEST<br>";
		msg += "<input name='text1' id=text1Id' type='text'><br>";
		msg += "<input type='submit' value='submit'><br>";
		msg += hr.getPostResult();
		msg += "</form>";

		msg += "<form action='search' method='GET'><br>";
		msg += "** SEARCH FILE TEST<br>";
		msg += "<input name='text2' id=text2Id' type='text'><br>";
		msg += "<input type='submit' value='submit'><br>";
		msg += fileSearchResult;
		msg += "</form>";

		msg += "<hr><a href='.'>refresh</a>";

		hr.setPostResult("");
		fileSearchResult = "";
	}

	public void errorBodySetting() {
		msg = "/"+hr.getUrl() + " :  POST URL !! NO GET REQUEST!!!!!<br>";
		msg += "<a href='.'>back</a>";
	}
}
