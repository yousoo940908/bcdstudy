package soo.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLDecoder;

import soo.http.HttpRequest;

public class HeaderParser {


	public void parsing(InputStream in, HttpRequest hr) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(in));
        
		String message = "";
		int contentLength = 0;
		
		while ((message = br.readLine())!= null) {   
			message = URLDecoder.decode(message, "utf-8");
			System.out.println(message);
			
			if(message.contains("HTTP/1.1")) {
				String[] splits = message.split(" ");
				hr.setMethod(splits[0]);
				
				if(hr.getMethod().equals("GET")) {
					if(!splits[1].contains("?")) {
						hr.setUrl(splits[1].substring(1));
						hr.setGetParam("");
						hr.setGetValue("");
					}else {
						String[] splits2 = splits[1].split("\\?");
						hr.setUrl(splits2[0].substring(1));
						hr.setGetParam(splits2[1].split("=")[0]);
						hr.setGetValue(splits2[1].split("=")[1]);
					}
				}
			}else if(message.contains("Content-Length")) {
				String[] splits = message.split(" ");
				contentLength = Integer.parseInt(splits[1]);
			}

			if("".equalsIgnoreCase(message))	break;
		}
		
		// post body ���� 
		char[] b = new char[contentLength+1];
		if(contentLength > 0) {
			br.read(b, 0, contentLength);
			hr.setPostResult("post value = " + URLDecoder.decode(new String(b).split("=")[1], "utf-8"));
		}
	}
}
