package soo.util;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import java.net.URL;

public class FileLoad {
	static String filePath = "C:/Users/soo/Downloads";
	
	public static void fileOutputStreamSetting(Socket socket, OutputStream out, String filename) throws IOException {
		URL resource = ClassLoader.getSystemResource(filename);
		File file = new File(resource.getPath());
		byte b[] = new byte[(int)file.length()];

		BufferedInputStream fin = new BufferedInputStream(new FileInputStream(file));
		int read = 0;
		out.write(new String ("Content-Length:"+b.length+"\r\n").getBytes());
		out.write(new String ("Content-Type: image/png;charset=UTF-8\r\n\r\n").getBytes());

		while ((read=fin.read(b))!=-1)	
			out.write(b,0,read);

		out.flush();
		out.close();
	}

	public static String findFileList(String searchWord) throws IOException {
		File file = new File(filePath);

		FilenameFilter filter = new FilenameFilter() {
			public boolean accept(File f, String name) {
				return name.contains(searchWord);
			}
		};

		String[] files = file.list(filter);
		
		String fileResult = "";
		for(String filename : files) {
			fileResult += filename + "<br>";
		}

		return fileResult;
	}
}
