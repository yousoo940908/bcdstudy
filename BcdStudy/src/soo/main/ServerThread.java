package soo.main;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

import soo.http.HttpRequest;
import soo.http.HttpResponse;

public class ServerThread extends Thread {
	Socket socket;

	public ServerThread(Socket socket) {
		this.socket = socket;
	}

	@Override
	public void run() {
		try {
			InputStream in = socket.getInputStream();
			HttpRequest hr = new HttpRequest();
			hr.receive(in); // inputStream ���� (method, url, postResut)

			OutputStream out = socket.getOutputStream();
			HttpResponse hps = new HttpResponse(hr);
			hps.send(socket, out); // outStream ���� 
			
		}catch (Exception e) {
			e.printStackTrace();
		}
	}


}
