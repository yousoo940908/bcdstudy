package soo.main;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;


public class Server {

	public static void main(String[] args) {

		try {
			ServerSocket serverSocket = new ServerSocket(80);
			System.out.println("=====================> START! ");


			while(true) {
				try {
					Socket socket = serverSocket.accept();
					System.out.println("=====> open : " + socket);

					new ServerThread(socket).start();
				}
				catch (Exception e) {
					e.printStackTrace();
				}
			}

		} catch (IOException e1) {
			e1.printStackTrace();
		}

	}

}
