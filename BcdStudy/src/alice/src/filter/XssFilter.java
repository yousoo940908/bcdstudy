package alice.src.filter;

import alice.src.http.HttpRequestMessage;

public class XssFilter {

	public static boolean passXssFilter(HttpRequestMessage req) {
		boolean passXssFilter  = false;
		if(req.getMethod().equals("POST")) {
			String tempBody = req.getBodyString();
			tempBody = tempBody.replaceAll("&", "&amp;");
			tempBody = tempBody.replaceAll("<", "&lt;");
			tempBody = tempBody.replaceAll(">", "&gt;");
			tempBody = tempBody.replaceAll("'", "&#x27;");
			tempBody = tempBody.replaceAll("\"", "&quot;");
			tempBody = tempBody.replaceAll("\\(", "&#40;");
			tempBody = tempBody.replaceAll("\\)", "&#41;");
			tempBody = tempBody.replaceAll("/", "&#x2F;");
			req.setBodyString(tempBody);
			passXssFilter = true;
			
		}
		else if(req.getMethod().equals("GET")) {
			passXssFilter = true;
		}
		return passXssFilter;
	}
}
