package alice.src.http;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLDecoder;
import java.util.HashMap;

public class HttpRequestMessage {

	String method;
	String url;
	String host;
	String connection;
	String secChUa;
	String secChUaMobile;
	String secChUaPlaform;
	String upgardeInsecureRequests;
	String userAgent;
	String accept;
	String secFetchSite;
	String secFetchMode;
	String secFetchUser;
	String secFetchDest;
	String referer;
	String acceptEncoding;
	String acceptLanguage;
	int contentLength;
	String bodyString;
	HashMap<String, String> getParam;

	HttpRequestMessage(){}
	public HttpRequestMessage(InputStream is){

		InputStreamReader isr = new InputStreamReader(is);
		BufferedReader reader = new BufferedReader(isr);

		String line = null;
		try {

			line = reader.readLine();
			String[] temp = line.split(" ");
			method = temp[0];
			url = temp[1];

			while (line != null && !line.equals("")) {
				
				if (line.matches("Host(.*)")) {
					host = line.substring(line.indexOf(":") + 1).trim();
				}
				if (line.matches("Connection(.*)")) {
					connection = line.substring(line.indexOf(":") + 1).trim();
				}
				if (line.matches("sec-ch-ua(.*)")) {
					secChUa = line.substring(line.indexOf(":") + 1).trim();
				}
				if (line.matches("sec-ch-ua-mobile(.*)")) {
					secChUaMobile = line.substring(line.indexOf(":") + 1).trim();
				}
				if (line.matches("sec-ch-ua-platform(.*)")) {
					secChUaPlaform = line.substring(line.indexOf(":") + 1).trim();
				}
				if (line.matches("Upgrade-Insecure-Requests(.*)")) {
					upgardeInsecureRequests = line.substring(line.indexOf(":") + 1).trim();
				}
				if (line.matches("User-Agent(.*)")) {
					userAgent = line.substring(line.indexOf(":") + 1).trim();
				}
				if (line.matches("Accept(.*)")) {
					accept = line.substring(line.indexOf(":") + 1).trim();
				}
				if (line.matches("Sec-Fetch-Site(.*)")) {
					secFetchSite = line.substring(line.indexOf(":") + 1).trim();
				}
				if (line.matches("Sec-Fetch-Mode(.*)")) {
					secFetchMode = line.substring(line.indexOf(":") + 1).trim();
				}
				if (line.matches("Sec-Fetch-User(.*)")) {
					secFetchUser = line.substring(line.indexOf(":") + 1).trim();
				}
				if (line.matches("Sec-Fetch-Dest(.*)")) {
					secFetchDest = line.substring(line.indexOf(":") + 1).trim();
				}
				if (line.matches("Referer(.*)")) {
					referer = line.substring(line.indexOf(":") + 1).trim();
				}
				if (line.matches("Accept-Encoding(.*)")) {
					acceptEncoding = line.substring(line.indexOf(":") + 1).trim();
				}
				if (line.matches("Accept-Language(.*)")) {
					acceptLanguage = line.substring(line.indexOf(":") + 1).trim();
				}
				if(line.matches("Content-Length(.*)")){
					contentLength = Integer.parseInt(line.substring(line.indexOf(":") + 1).trim());
				}
//				System.out.println(line);
				line = reader.readLine();
			}

			if(method.equals("POST")){
				char[] buffer = new char[contentLength];
				reader.read(buffer, 0, contentLength);
				String tempStr = "";
				for(int i = 0; i<buffer.length; i++){
					tempStr = tempStr + buffer[i];
				}
				bodyString = URLDecoder.decode(tempStr, "utf-8");
			}
			else if(method.equals("GET") && url.matches("(.*)?(.*)=(.*)")){
				String[] getTemp = url.split("\\?");
				url = getTemp[0];
				String getParamStr = getTemp[1];
				getTemp = getParamStr.split("&");
				if(getTemp.length > 0) {
					getParam = new HashMap<String,String>();
					
					for(int i = 0; i< getTemp.length; i++) {
						String[] keyValue = getTemp[i].split("=");
						String key = URLDecoder.decode(keyValue[0], "utf-8");
						String val = URLDecoder.decode(keyValue[1], "utf-8");
						getParam.put(key, val);	
					}
				}
			}

		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	public String getMethod() {
		return method;
	}
	public void setMethod(String method) {
		this.method = method;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getHost() {
		return host;
	}
	public void setHost(String host) {
		this.host = host;
	}
	public String getConnection() {
		return connection;
	}
	public void setConnection(String connection) {
		this.connection = connection;
	}
	public String getSecChUa() {
		return secChUa;
	}
	public void setSecChUa(String secChUa) {
		this.secChUa = secChUa;
	}
	public String getSecChUaMobile() {
		return secChUaMobile;
	}
	public void setSecChUaMobile(String secChUaMobile) {
		this.secChUaMobile = secChUaMobile;
	}
	public String getSecChUaPlaform() {
		return secChUaPlaform;
	}
	public void setSecChUaPlaform(String secChUaPlaform) {
		this.secChUaPlaform = secChUaPlaform;
	}
	public String getUpgardeInsecureRequests() {
		return upgardeInsecureRequests;
	}
	public void setUpgardeInsecureRequests(String upgardeInsecureRequests) {
		this.upgardeInsecureRequests = upgardeInsecureRequests;
	}
	public String getUserAgent() {
		return userAgent;
	}
	public void setUserAgent(String userAgent) {
		this.userAgent = userAgent;
	}
	public String getAccept() {
		return accept;
	}
	public void setAccept(String accept) {
		this.accept = accept;
	}
	public String getSecFetchSite() {
		return secFetchSite;
	}
	public void setSecFetchSite(String secFetchSite) {
		this.secFetchSite = secFetchSite;
	}
	public String getSecFetchMode() {
		return secFetchMode;
	}
	public void setSecFetchMode(String secFetchMode) {
		this.secFetchMode = secFetchMode;
	}
	public String getSecFetchUser() {
		return secFetchUser;
	}
	public void setSecFetchUser(String secFetchUser) {
		this.secFetchUser = secFetchUser;
	}
	public String getSecFetchDest() {
		return secFetchDest;
	}
	public void setSecFetchDest(String secFetchDest) {
		this.secFetchDest = secFetchDest;
	}
	public String getReferer() {
		return referer;
	}
	public void setReferer(String referer) {
		this.referer = referer;
	}
	public String getAcceptEncoding() {
		return acceptEncoding;
	}
	public void setAcceptEncoding(String acceptEncoding) {
		this.acceptEncoding = acceptEncoding;
	}
	public String getAcceptLanguage() {
		return acceptLanguage;
	}
	public void setAcceptLanguage(String acceptLanguage) {
		this.acceptLanguage = acceptLanguage;
	}
	public int getContentLength() {
		return contentLength;
	}
	public void setContentLength(int contentLength) {
		this.contentLength = contentLength;
	}
	public String getBodyString() {
		return bodyString;
	}
	public void setBodyString(String bodyString) {
		this.bodyString = bodyString;
	}
	public String getGetParamVal(String key) {
		return getParam.get(key);
	}
	
	public HashMap<String,String> getGetParam() {
		return getParam;
	}
	
}