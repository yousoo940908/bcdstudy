package alice.src.http;

public class HttpResponseMessage {

	String StatusLine;
	String Headers;
	String Body;

	byte[] fileByte;
	public String getStatusLine() {
		return StatusLine;
	}
	public void setStatusLine(String statusLine) {
		StatusLine = statusLine;
	}
	public String getHeaders() {
		return Headers;
	}
	public void setHeaders(String headers) {
		Headers = headers;
	}
	public String getBody() {
		return Body;
	}
	public void setBody(String body) {
		Body = body;
	}
	public byte[] getFileByte() {
		return fileByte;
	}
	public void setFileByte(byte[] fileByte) {
		this.fileByte = fileByte;
	}
	public String getResponseHeaderMsg() {
		String res = "";
		res = res + StatusLine + "\r\n";
		res = res + Headers + "\r\n";
		res = res + "\r\n";
		return res;
	}

}
