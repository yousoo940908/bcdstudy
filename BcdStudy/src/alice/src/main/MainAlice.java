package alice.src.main;

import java.net.ServerSocket;
import java.net.Socket;

public class MainAlice {

	public static void main(String[] args) {
		try{
			ServerSocket server = new ServerSocket(8080);
			System.out.println("open alice server");
			
			while(true){
				final Socket client = server.accept();
				if(client.isConnected()) {
					System.out.println("connected alice server");
					ReturnService rs = new ReturnService();
					rs.run(client);
				}
				client.close();
			}
		}catch(Exception e){
			System.out.println(e);
		}
	}

}

