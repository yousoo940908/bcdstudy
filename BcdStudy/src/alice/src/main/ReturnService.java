package alice.src.main;

import java.io.*;
import java.net.Socket;

import alice.src.filter.XssFilter;
import alice.src.http.HttpRequestMessage;
import alice.src.http.HttpResponseMessage;
import alice.src.service.HttpService;
import alice.src.utlz.FileUtil;

public class ReturnService extends Thread{
	
	public Socket run(Socket client) {
		try {

			HttpRequestMessage req = new HttpRequestMessage(client.getInputStream());
			
			// start filter 처리 하기 
			if(!XssFilter.passXssFilter(req)) {
				throw new Exception();
			}
			// end filter 처리하기
			
			DataOutputStream dos = new DataOutputStream(client.getOutputStream());
			HttpService httpService = new HttpService(req);
			HttpResponseMessage  res = httpService.returnService();
			dos.write(res.getResponseHeaderMsg().getBytes("UTF-8"));
			
			if(req.getSecFetchDest().equals("image")){ // image
				dos.write(res.getFileByte());
			}
			else{
				dos.write(res.getBody().getBytes("UTF-8"));
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return client;
	}
}
