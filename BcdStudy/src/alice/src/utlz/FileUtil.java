package alice.src.utlz;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

public class FileUtil {
	
	public byte[] readImg(String path) {

		File f1 = new File(path);
		byte[] buffer = null;

		try {
			FileInputStream fis = new FileInputStream(f1);
			BufferedInputStream bis = new BufferedInputStream(fis);
			try {
				buffer = new byte[bis.available()];
				bis.read(buffer);
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return buffer;
	}
	
	public String readHtml(String path) {

		File f1 = new File(path);
		String readHtml = "";

		try {
			FileInputStream fis = new FileInputStream(f1);
			BufferedInputStream bis = new BufferedInputStream(fis);
			
			FileReader fr = new FileReader(f1);
			BufferedReader br = new BufferedReader(fr);
			
			
			String str = null;
			while((str = br.readLine()) != null) {
				readHtml += str;
			}
			br.close();
			fr.close();
		    
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return readHtml;
	}
	
	public String readHtml(String path, String inputVal) {

		File f1 = new File(path);
		String readHtml = "";

		try {
			FileInputStream fis = new FileInputStream(f1);
			BufferedInputStream bis = new BufferedInputStream(fis);
			
			FileReader fr = new FileReader(f1);
			BufferedReader br = new BufferedReader(fr);
			
			
			String str = null;
			while((str = br.readLine()) != null) {
				if(str.contains("</body>")){
					readHtml += "<h1>" + inputVal + "</h1>";
				}
				readHtml += str;
			}
			br.close();
			fr.close();
		    
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return readHtml;
	}
	
	public String readHtml(String path,HashMap<String,String> inputVal) {

		File f1 = new File(path);
		String readHtml = "";

		try {
			FileInputStream fis = new FileInputStream(f1);
			BufferedInputStream bis = new BufferedInputStream(fis);
			
			FileReader fr = new FileReader(f1);
			BufferedReader br = new BufferedReader(fr);
			
			
			String str = null;
			while((str = br.readLine()) != null) {
				if(str.contains("</body>")){
					readHtml += "<h1>" + returnFileName(inputVal.get("condition")) + "</h1>";
				}
				readHtml += str;
			}
			br.close();
			fr.close();
		    
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return readHtml;
	}
	
	private String returnFileName(String findName) {
		String fileName = "파일이 없습니다.";
		File dir  = new File(System.getProperty("user.dir") + "/src/alice");
		String[] str = dir.list();
		for(int i = 0; i<str.length; i++) {
			if(str[i].contains(findName)) {
				fileName = str[i];
				break;
			}
		}
		return fileName;
	}
}
