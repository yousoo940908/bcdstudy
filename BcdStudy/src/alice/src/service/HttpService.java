package alice.src.service;

import alice.src.http.HttpRequestMessage;
import alice.src.http.HttpResponseMessage;
import alice.src.utlz.FileUtil;

public class HttpService {

	private HttpRequestMessage req;
	private HttpResponseMessage res;
	private FileUtil fUtil;
	private String htmlPath = System.getProperty("user.dir") + "/src/alice/resource/html/";
	private String imgPath = System.getProperty("user.dir") + "/src/alice/resource/img";
	
	HttpService(){
	}
	
	public HttpService(HttpRequestMessage req){
		this.req = req;
		this.fUtil = new FileUtil();
		this.res = new HttpResponseMessage();
	}
	
	public HttpResponseMessage returnService() {
		
		if(this.req.getSecFetchDest().equals("image")){ // image
			String filePath = imgPath + this.req.getUrl();
			byte[] readFileByte = this.fUtil.readImg(filePath);
			this.res.setFileByte(readFileByte);

			String statusLine = "HTTP/1.1 200 OK";
			this.res.setStatusLine(statusLine);

			String header = "";
			header = "Content-type: image/png\r\n";
			header = header + "Content-Length: " + readFileByte.length;
			this.res.setHeaders(header);
		}
		else { // doc
			if(this.req.getMethod().equals("GET") && this.req.getUrl().equals("/")) {
				String msg = fUtil.readHtml(htmlPath + "index.html");
				this.res.setBody(msg);

				String statusLine = "HTTP/1.1 200 OK";
				this.res.setStatusLine(statusLine);

				String header = "";
				header = "Content-type: text/html;charset=UTF-8";
				this.res.setHeaders(header);
			}
			else if(this.req.getMethod().equals("POST") && this.req.getUrl().equals("/ABC")){
				String msg = fUtil.readHtml(htmlPath + "index.html", this.req.getBodyString());
				this.res.setBody(msg);

				String statusLine = "HTTP/1.1 200 OK";
				this.res.setStatusLine(statusLine);

				String header = "";
				header = "Content-type: text/html;charset=UTF-8";
				this.res.setHeaders(header);
			}
			else if(this.req.getMethod().equals("GET") && this.req.getUrl().equals("/SEARCH")){
				String msg = fUtil.readHtml(htmlPath + "index.html", this.req.getGetParam());
				this.res.setBody(msg);

				String statusLine = "HTTP/1.1 200 OK";
				this.res.setStatusLine(statusLine);

				String header = "";
				header = "Content-type: text/html;charset=UTF-8";
				this.res.setHeaders(header);
			}
			else {
				String msg = htmlPath + "accessDenied.html";
				this.res.setBody(msg);

				String statusLine = "HTTP/1.1 403 Forbidden";
				this.res.setStatusLine(statusLine);

				String header = "";
				header = "Content-type: text/html;charset=UTF-8";
				this.res.setHeaders(header);
			}
		}
		
		return this.res;
	}
}
