package dew;

import java.io.BufferedReader;
        import java.io.File;
        import java.io.FileInputStream;
        import java.io.IOException;
        import java.io.InputStream;
        import java.io.InputStreamReader;
        import java.io.OutputStream;
        import java.net.ServerSocket;
        import java.net.Socket;

public class SimpleServer {

    public final static int port = 80;
    public final static int BUFFER_SIZE = 256;
    public final static String post_url1 = "bark";
    public final static String text_header = "HTTP/2.0 200 document follows \r\n"
            + "Content-Type: text/html; charset=utf-8 \r\n" + "\r\n";
    public final static File imgFile_lion = new File(
            "/Users/kakaobank/Documents/workspace-spring-tool-suite-4-4.17.0.RELEASE/dewsocket/src/lion.jpg");

    public static void imgProcess(OutputStream output) throws IOException {

        final FileInputStream fis = new FileInputStream(imgFile_lion);
        final byte[] buffer = new byte[BUFFER_SIZE];
        int length;
        while ((length = fis.read(buffer)) != -1)
            output.write(buffer, 0, length);
        fis.close();
    }

    public static void txtProcess(OutputStream output) throws IOException {

        String body = "<br>test page</br> " + "<img src = 'lion.jpg'/>";
        String formBody = "<form action='/" + post_url1 + "' method='POST'/>"
                + "<br><input name='a' id='b' type=\"text\"/>" + "<br><input type='submit' value='제출'/>";

        output.write((text_header + body + formBody).getBytes());
    }

    public static void postProcess(OutputStream output, BufferedReader reader, int length) throws IOException {

        final char[] temp = new char[length];
        reader.read(temp, 0, length);
        System.out.println("############# POST BODY : " + String.valueOf(temp));

        final String body = "<br>test page</br> " + "<img src = 'lion.jpg'/>";
        final String formBody = "<form action='/" + post_url1 + "' method='POST'/>"
                + "<br><input name='a' id='b' type=\"text\"/>" + "<br><input type='submit' value='제출'/>" + "<br>"
                + String.valueOf(temp) + "</br>";

        output.write((text_header + body + formBody).getBytes());
    }

    public static void showErrorPage(OutputStream output) throws IOException {

        final String body = "<br>ERROR</br> <a href='/'>사이트로 이동!</a>"
                + "<script>"
                + "const fruits = ['apples', 'bananas', 'cherries'];\n"
                + "for (const fruit of fruits) {\n"
                + "  console.log(fruit);\n"
                + "}\n"
                + "</script>";
        output.write((text_header + body).getBytes());
    }

    public static MethodTypes defineFlag(String tmp) {
        MethodTypes flag = MethodTypes.GET;
        if (tmp == null) {
            return flag;
        }
        if (tmp.startsWith("POST")) {
            flag = MethodTypes.POST;
        } else {
            if (tmp.contains("jpg"))
                flag = MethodTypes.GET_IMAGE;
            if (tmp.split("/")[1].startsWith(post_url1))
                flag = MethodTypes.ERROR_PAGE;
        }
        System.out.println("flag : " + flag);
        return flag;
    }

    public static void main(String[] args) {

        MethodTypes flag = MethodTypes.GET;

        try (ServerSocket serverSocket = new ServerSocket(port)) {

            System.out.println("############# Server start with port number [" + port + "]");

            while (true) {
                Socket socket = serverSocket.accept();
                System.out.println("############# " + socket.getInetAddress() + " client connected");
                final OutputStream output = socket.getOutputStream();
                final InputStream input = socket.getInputStream();
                final BufferedReader reader = new BufferedReader(new InputStreamReader(input));

                int length = 0;
                String tmp = reader.readLine();
                flag = defineFlag(tmp);

                while (tmp != null) {
                    if (flag.equals(MethodTypes.POST) && tmp.startsWith("Content-Length")) {
                        length = Integer.valueOf(tmp.split(" ")[1]);
                    }
                    if ("".equals(tmp)) {
                        System.out.println("############# HTTP Header end.. ");
                        break;
                    }
                    System.out.println("" + tmp);
                    tmp = reader.readLine();
                } // end while(IN)

                switch (flag) {
                    case GET:
                        txtProcess(output);
                        break;
                    case GET_IMAGE:
                        imgProcess(output);
                        break;
                    case POST:
                        postProcess(output, reader, length);
                        break;
                    case ERROR_PAGE:
                        showErrorPage(output);
                        break;
                    default:
                        break;
                }
                System.out.println("현재 서버 스레드 : " + Thread.currentThread());
                System.out.println("현재 서버 pid : " + ProcessHandle.current().pid());
                output.close();
                socket.close();
            }
        } catch (IOException ex) {
            System.out.println("Server exception: " + ex.getMessage());
            ex.printStackTrace();
        }
    }
}

/*
 * 문제1 reader.readLine()을 하면 에러 발생 이유) 클라이언트로부터 받아오는 정보가 없기 때문임 문제2
 * reader.readLine()을 하지 않으면 대기중 뜸 이유) POST를 두 번 날리는데 클라이언트에서 접속을 하지 않음, 이 경우
 *
 * 서버에서 클라이언트로 Response(OutputStream) 해야 함 POST는 GET와 다르게 BODY를 가짐
 */