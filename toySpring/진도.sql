1. 2023-01-20
   ㄴ스프링 기본 구조(core, mvc, jdbc 등등..)
    -인터셉터,aop
   ㄴ실행순서 구조 https://dadadamarine.github.io/java/spring/2019/05/02/spring-MVC-%EA%B5%AC%EC%A1%B0.html
    -우리가 흔히 쓰는 RequestMapping https://liamkim-daeyong.github.io/posts/spring-16/
    - viewresolver
    - modelandview
   ㄴ싱글턴에대한 내용(멀티쓰레드 환경에서 100만명이 요청하면.. 객체 100만개 생성!? ㄴㄴ 하나만 씀 그러면 thread 동시성문제는!?)
   ㄴ쓰레드풀(servlet), db의 쓰레드풀dbcp(hikari, apacheCommon, tomcatDBCP)
   ㄴweb기반이 아닌 java기반의 스프링 익혀보기
   ㄴXML, annotation 기반 익히기
 
   
2. 2023-01-27
   ㄴ웹 기본 구조 만들어보기(controller, service, mapper)
   ㄴ우리가 프로젝트를 구성하면서 사용해야 할 기능들은 뭐가 있을까?
    Ex) 1. DB 
          1.1 DB의 트랜잭션 관리 등..
        2. 또뭐가 있을까요?(숙제)
        
   ㄴ직접 사용해야 할 기능들을 붙여보기 각자 하나씩 담당하여 만들어보고 가이드하기
   
