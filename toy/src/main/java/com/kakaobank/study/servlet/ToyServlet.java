package com.kakaobank.study.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class ToyServlet
 */
@WebServlet(value = "/toyServlet")
public class ToyServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * Default constructor. 
     */
    public ToyServlet() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		// 1첫번째 서비스
//		ToyService service = new ToyService("a","b");
		System.out.println("야호"+request.getParameter("asdf"));
		/*
		 * step1. 비즈니스 로직 처리
		 * 조회하고.. ~~ DB INSERT하고 등등 할거 다하고 
		 * */
		
		String userName = "toy.w";
		/*
		 * step2. 최종적으로 화면 출력
		 * */
//		response.getWriter().append("<html>").append("<head></head> <body><input type=text value=123 />");
//		response.getWriter().append(userName).append("</body></html>");
		
		request.setAttribute("name", "나는바보");
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("/jsp/a.jsp");
		requestDispatcher.forward(request, response);
		
//		response.sendRedirect("/toy/jsp/a.jsp");
		
	}
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}
}
